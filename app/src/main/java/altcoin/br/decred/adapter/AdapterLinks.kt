package altcoin.br.decred.adapter

import altcoin.br.decred.R
import altcoin.br.decred.model.Link
import altcoin.br.decred.utils.Utils
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class AdapterLinks(private val context: Context, private val links: List<Link>) : BaseAdapter() {

	override fun getCount() = links.size

	override fun getItem(i: Int) = links[i]

	override fun getItemId(i: Int) = i.toLong()

	override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
		val li = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

		val v = li.inflate(R.layout.row_links, null)

		val tvLinkLabel = v.findViewById(R.id.tvLinkLabel) as TextView
		val tvLinkUrl = v.findViewById(R.id.tvLinkUrl) as TextView

		var label = links[position].label

		if (!label.endsWith(":"))
			label += ":"

		tvLinkLabel.text = label.trim { it <= ' ' }
		tvLinkUrl.text = links[position].url.trim { it <= ' ' }

		Utils.textViewLink(tvLinkUrl, links[position].url)

		return v
	}
}
